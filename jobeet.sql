-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Dim 17 Janvier 2016 à 17:21
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `jobeet`
--

-- --------------------------------------------------------

--
-- Structure de la table `jobeet_affiliate`
--

CREATE TABLE IF NOT EXISTS `jobeet_affiliate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `jobeet_affiliate_U_1` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `jobeet_category`
--

CREATE TABLE IF NOT EXISTS `jobeet_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `jobeet_category_U_1` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `jobeet_category`
--

INSERT INTO `jobeet_category` (`id`, `name`) VALUES
(4, 'Administrator'),
(1, 'Design'),
(3, 'Manager'),
(2, 'Programming');

-- --------------------------------------------------------

--
-- Structure de la table `jobeet_category_affiliate`
--

CREATE TABLE IF NOT EXISTS `jobeet_category_affiliate` (
  `category_id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`affiliate_id`),
  KEY `jobeet_category_affiliate_FI_2` (`affiliate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `jobeet_job`
--

CREATE TABLE IF NOT EXISTS `jobeet_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `company` varchar(255) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `position` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `how_to_apply` text NOT NULL,
  `token` varchar(255) NOT NULL,
  `is_public` tinyint(4) NOT NULL DEFAULT '1',
  `is_activated` tinyint(4) NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL,
  `expires_at` datetime NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `jobeet_job_U_1` (`token`),
  KEY `jobeet_job_FI_1` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `jobeet_job`
--

INSERT INTO `jobeet_job` (`id`, `category_id`, `type`, `company`, `logo`, `url`, `position`, `location`, `description`, `how_to_apply`, `token`, `is_public`, `is_activated`, `email`, `expires_at`, `created_at`, `updated_at`) VALUES
(1, 2, 'full-time', 'Sensio Labs', 'sensio-labs.gif', 'http://www.sensiolabs.com/', 'Web Developer', 'Paris, France', 'You''ve already developed websites with symfony and you want to\nwork with Open-Source technologies. You have a minimum of 3\nyears experience in web development with PHP or Java and you\nwish to participate to development of Web 2.0 sites using the\nbest frameworks available.\n', 'Send your resume to fabien.potencier [at] sensio.com\n', 'job_sensio_labs', 1, 1, 'job@example.com', '2010-10-10 00:00:00', '2016-01-17 16:23:03', '2016-01-17 16:23:03'),
(2, 1, 'part-time', 'Extreme Sensio', 'extreme-sensio.gif', 'http://www.extreme-sensio.com/', 'Web Designer', 'Paris, France', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do\neiusmod tempor incididunt ut labore et dolore magna aliqua. Ut\nenim ad minim veniam, quis nostrud exercitation ullamco laboris\nnisi ut aliquip ex ea commodo consequat. Duis aute irure dolor\nin reprehenderit in.\n\nVoluptate velit esse cillum dolore eu fugiat nulla pariatur.\nExcepteur sint occaecat cupidatat non proident, sunt in culpa\nqui officia deserunt mollit anim id est laborum.\n', 'Send your resume to fabien.potencier [at] sensio.com\n', 'job_extreme_sensio', 1, 1, 'job@example.com', '2010-10-10 00:00:00', '2016-01-17 16:23:03', '2016-01-17 16:23:03');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `jobeet_category_affiliate`
--
ALTER TABLE `jobeet_category_affiliate`
  ADD CONSTRAINT `jobeet_category_affiliate_FK_1` FOREIGN KEY (`category_id`) REFERENCES `jobeet_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jobeet_category_affiliate_FK_2` FOREIGN KEY (`affiliate_id`) REFERENCES `jobeet_affiliate` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `jobeet_job`
--
ALTER TABLE `jobeet_job`
  ADD CONSTRAINT `jobeet_job_FK_1` FOREIGN KEY (`category_id`) REFERENCES `jobeet_category` (`id`);
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
